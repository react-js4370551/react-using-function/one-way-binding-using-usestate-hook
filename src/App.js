
import './App.css';
import {OneWayDataBiningUsingUseState} from './component/one-way-data-binding-using-usestate';

function App() {
  return (
    <div className="App">
      <OneWayDataBiningUsingUseState />
    </div>
  );
}

export default App;
