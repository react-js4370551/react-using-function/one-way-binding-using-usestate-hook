import React, { useState } from 'react'

export function OneWayDataBiningUsingUseState() {
  //in react it is recommondate to never initialze variable with var or let(immutable variable)
  //initialise with variable using useState.
  const [name] = useState("Ashish");
  return (
    <div>
        <h1>OneWayDataBiningUsingUseState</h1>
        <p>Hi ! {name}</p>
    </div>
  )
}
